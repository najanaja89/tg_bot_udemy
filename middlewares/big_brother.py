import logging

from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware

class BigBrother(BaseMiddleware):
    async def on_pre_process_update(self, update: types.Update, data:dict):
        logging.info("[---------------New Update--------------]")
        logging.info("1. Pre Process Update")
        logging.info("Next Point Process update")
        data["middleware_data"] = "this come through on_post_process_update"
        if update.message:
            user = update.message.from_user.id
        elif update.callback_query:
            user=update.callback_query.from_user.id
        else:
            return

    async def on_process_update(self, update: types.Update, data:dict):
        logging.info(f"2. Process Update, {data=})")
        logging.info(f"Next Point: Pre Process Message")

    async def on_pre_process_message(self, message:types.Message, data: dict):
        logging.info(f"3.Pre Process Message, {data=})")
        logging.info(f"Next Point: Filters")
        data["middleware_data"] = "this come through process_message"

    async def on_process_message(self, message: types.Message, data:dict):
        logging.info(f"5.Process Message")
        logging.info(f"Next Point: Handler")
        data["middleware_data"] = "this come through handler"







