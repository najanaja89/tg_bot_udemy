from aiogram import types

from aiogram.dispatcher.filters.builtin import CommandStart

from filters.test_filter import SomeF
from loader import dp


@dp.message_handler(CommandStart(), SomeF() )
async def bot_start(message: types.Message, middleware_data, from_filter):
    await message.answer(f'Привет, {message.from_user.full_name}! {middleware_data=} {from_filter=}')



