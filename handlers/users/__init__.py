from .help import dp
from .purchase import dp
from .task2 import dp
from .testing import dp
from .start import dp
from .echo import dp


__all__ = ["dp"]
