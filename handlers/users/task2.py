from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command, state
from loader import dp
from aiogram import types
from states import Task2


@dp.message_handler(Command("form"), state=None)
async def enter_test(message: types.Message):
    await message.answer("Введите Имя")

    await Task2.QName.set()

@dp.message_handler(state=Task2.QName)
async def answer_qname(message: types.Message, state: FSMContext):
    answer = message.text
    await state.update_data(answerName = answer)

    await message.answer("Введите email")
    await Task2.next()

@dp.message_handler(state=Task2.QEmail)
async def answer_qemail(message: types.Message, state: FSMContext):
    answer = message.text
    await state.update_data(answerEmail = answer)

    await message.answer("Введите номер")
    await Task2.next()

@dp.message_handler(state=Task2.QPhone)
async def answer_qemail(message: types.Message, state: FSMContext):

    data = await state.get_data()
    name = data.get("answerName")
    email = data.get("answerEmail")
    number = message.text


    await message.answer(f"Привет! Ты ввел следующие данные:\nИмя - {name}\nEmail - {email}\nТелефон - {number}")

    await state.reset_state()


